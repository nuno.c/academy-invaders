package org.academiadecodigo.stringteasers.academyinvaders.ammunition;


import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Character;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Enemy;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Player;

/**
 * Created by codecadet on 17/10/2019.
 */
public class Bullet extends Ammunition {

    public int damage = 2;
    public final static int SPEED = 5;

    private int xPosition;
    private int yPosition;
    private Picture bulletImage;
    private boolean destroyed;

    //CONSTRUCTOR OVERLOAD
    public Bullet(Player player, int xPosition, int yPosition) {
        this.xPosition = xPosition + 35;
        this.yPosition = yPosition;
        destroyed = false;
        bulletImage = new Picture(this.xPosition, this.yPosition, "resources/images/playerbullet.png");
        bulletImage.draw();
    }

    public Bullet(Enemy enemy, int xPosition, int yPosition ) {
        this.xPosition = xPosition + 35;
        this.yPosition = yPosition + 80;
        destroyed = false;
        bulletImage = new Picture(this.xPosition, this.yPosition, "resources/images/enemybullet.png");
        bulletImage.draw();
    }

    public void deleteImage(){
        bulletImage.delete();
    }

    public int getDamage(){
        return damage;
    }

    public void setHit() {
        destroyed = true;
    }

    public boolean getDestroyed(){return destroyed;}

    public int getyPosition() {
        return yPosition;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void motion() {
            if (bulletImage != null) {
                for (int i = 0; i < SPEED; i++) {
                    bulletImage.translate(0, -1);
                    yPosition -= 1;
                }
            }
        }

        //bulletImage.delete();
        //bulletImage.delete();

    public void motionEnemy(){
        if (bulletImage != null) {
            for (int i = 0; i < SPEED; i++) {
                bulletImage.translate(0, +1);
                yPosition += 1;
            }
        }

    }
}
