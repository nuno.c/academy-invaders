package org.academiadecodigo.stringteasers.academyinvaders.ammunition;

public enum AmmunitionType {
    BULLET,
    ROCKET
}

