package org.academiadecodigo.stringteasers.academyinvaders;

import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Bullet;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Enemy;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Meteor;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Player;
import org.academiadecodigo.stringteasers.academyinvaders.sounds.Sound;

import java.util.List;

// PROPERTIES

    public class CollisionDetector {
    private Player player;
    private List<Bullet> bulletList;
    private List<Bullet> enemyBulletList;
    private Enemy enemy;
    private Meteor[] meteors;
    private Sound sound;

    // CONSTRUCTOR

    public CollisionDetector(Player player, List<Bullet> bulletList, Enemy enemy, List<Bullet> enemyBulletList, Meteor[] meteors) {
        this.player = player;
        this.bulletList = bulletList;
        this.enemy = enemy;
        this.enemyBulletList = enemyBulletList;
        this.meteors = meteors;
    }

    // METHODS

    public void isEnemyHit() {
        for (Bullet bullet : bulletList) {
            if (!bullet.getDestroyed()) {
                if ((enemy.getxPosition() <
                        bullet.getxPosition() + 20)
                        && (enemy.getxPosition() + 80 > bullet.getxPosition())
                        && (enemy.getyPosition() < bullet.getyPosition() + 10)
                        && (enemy.getyPosition() + 80 > bullet.getyPosition())) {

                    bullet.setHit();
                    sound = new Sound("/resources/sounds/explosion.wav");
                    sound.play(true);
                    enemy.suffer(10);
                    //bullet.setHit();// bullet was destroyed
                    //bulletList.remove(bullet); BOOM
                }
            }
        }
    }

    public void isPlayerHit() {
        for (Bullet enemybullet : enemyBulletList) {
            if (!enemybullet.getDestroyed()) {
                if ((player.getxPosition() < enemybullet.getxPosition() + 20)
                        && (player.getxPosition() + 80 > enemybullet.getxPosition())
                        && (player.getyPosition() < enemybullet.getyPosition() + 10)
                        && (player.getyPosition() + 80 > enemybullet.getyPosition())) {
                    enemybullet.setHit();
                    player.suffer(10);
                }
            }

        }
    }

    public void isMeteorHit() {
        for (Bullet bullet : bulletList) {
            for (Meteor meteor : meteors) {
                if (!bullet.getDestroyed()) {
                    if ((meteor.getxPosition() <
                            bullet.getxPosition() + 20)
                            && (meteor.getxPosition() + 80 > bullet.getxPosition())
                            && (meteor.getYPosition() < bullet.getyPosition() + 10)
                            && (meteor.getYPosition() + 80 > bullet.getyPosition())) {
                        bullet.setHit();
                        meteor.suffer(10);
                    }
                }
            }
        }
    }

    public void isMeteorHitPlayer() {

        for (Meteor meteor : meteors) {
            if(!meteor.isDestroyed()){
            if ((meteor.getxPosition() < player.getxPosition() + 80)
                    && (meteor.getxPosition() + 80 > player.getxPosition())
                    && (meteor.getYPosition() < player.getyPosition() + 80)
                    && (meteor.getYPosition() + 80 > player.getyPosition())){
                System.out.println("inside collision enemy");
                player.suffer(100);
                sound = new Sound("/resources/sounds/explosion");
                meteor.setDestroyed();

            }
            }
        }
    }

}

