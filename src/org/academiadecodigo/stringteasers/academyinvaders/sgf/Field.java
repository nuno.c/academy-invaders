package org.academiadecodigo.stringteasers.academyinvaders.sgf;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Field {

    // PROPERTIES

    private Rectangle field;
    public final int MARGIN = 0;
    public final int PADDING = 10;
    public final int WIDTH = 800;
    public final int HEIGHT = 880;
    private Picture fieldBackground;

    // CONSTRUCTOR

    public Field() {

        field = new Rectangle(MARGIN, MARGIN, WIDTH, HEIGHT);
        fieldBackground = new Picture(this.field.getX(), this.field.getY(),"resources/images/background.png");

    }

    // METHODS
    public void init() {
        fieldBackground.draw();
    }
    public int getLeftBorder() {
        return PADDING;
    }

    public int getRightBorder() {
        return WIDTH;
    }

    public void endField() {
        fieldBackground = new Picture(this.field.getX(), this.field.getY(), "resources/images/gameover.png");
        fieldBackground.draw();
    }





}
