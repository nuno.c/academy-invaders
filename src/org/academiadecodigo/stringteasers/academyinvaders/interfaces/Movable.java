package org.academiadecodigo.stringteasers.academyinvaders.interfaces;

public interface Movable {
    void move();
}
