package org.academiadecodigo.stringteasers.academyinvaders.interfaces;

import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Ammunition;
import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Bullet;

public interface Hitable {
    void suffer(int damage);
}
