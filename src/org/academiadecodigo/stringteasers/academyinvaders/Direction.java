package org.academiadecodigo.stringteasers.academyinvaders;

public enum Direction {
    UP,
    DOWN,
    RIGHT,
    LEFT

}
