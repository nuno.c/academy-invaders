package org.academiadecodigo.stringteasers.academyinvaders.gameobject.character;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringteasers.academyinvaders.Direction;
import org.academiadecodigo.stringteasers.academyinvaders.sounds.Sound;

public class Meteor {

    // PROPERTIES

    private int health;
    private int damage;
    private Picture meteorImage;
    private int xPosition;
    private int yPosition;
    private int speed;
    private boolean destroyed;
    private Sound sound;


    // CONSTRUCTOR

    public Meteor(int xPosition, int yPosition) {

        this.xPosition = xPosition;
        this.yPosition = yPosition;
        speed = 0;
        meteorImage = new Picture(xPosition, this.yPosition, "images/meteor.png");
        meteorImage.draw();
        destroyed = false;
        health = 30;
    }

    // METHODS

    public void move() {
        accelerate(speed);
    }
    public boolean isDestroyed(){
        return destroyed;
    }
    public void setDestroyed(){
        destroyed = true;
        meteorImage.delete();
    }
    public void accelerate(int speed) {
        for (int i = 0; i < speed; i++) {
            moveDown(1);
        }

    }

    public void moveDown(int distance) {
        meteorImage.translate(0, distance);
        yPosition += 1;
    }

    public void deleteImage() {
        meteorImage.delete();
    }

    // SETTERS AND GETTERS

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getYPosition() {
        return yPosition;
    }

    public int getxPosition() { return xPosition; }



    public void suffer(int damage) {

        if (health - damage == 0) {
            this.destroyed = true;
            sound = new Sound("/resources/sounds/explosion.wav");
            sound.play(true);
            meteorImage.delete();


        }

        health -= damage;
    }



}
