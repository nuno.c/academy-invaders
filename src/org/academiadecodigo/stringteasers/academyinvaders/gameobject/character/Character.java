package org.academiadecodigo.stringteasers.academyinvaders.gameobject.character;

import org.academiadecodigo.stringteasers.academyinvaders.Game;
import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Ammunition;
import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Bullet;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.GameObject;

public abstract class Character extends GameObject {

    protected int health;
    protected int xPosition;
    protected int yPosition;
    protected Game game;
    protected int speed = 2;

    /*
     * Construtor
     */
    public Character(int health, Game game,int xPosition,int yPosition) {

        super();
        this.health = health;
        this.game = game;
        this.xPosition = xPosition;
        this.yPosition = yPosition;

    }

    @Override
    public void move() {

    }


    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

}
