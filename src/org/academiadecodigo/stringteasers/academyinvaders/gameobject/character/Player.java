package org.academiadecodigo.stringteasers.academyinvaders.gameobject.character;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringteasers.academyinvaders.Direction;
import org.academiadecodigo.stringteasers.academyinvaders.Game;
import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Bullet;
import org.academiadecodigo.stringteasers.academyinvaders.sgf.Field;
import org.academiadecodigo.stringteasers.academyinvaders.sounds.Sound;


import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent.KEY_LEFT;
import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent.KEY_RIGHT;
import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType.KEY_PRESSED;
import static org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType.KEY_RELEASED;

public class Player extends Character implements KeyboardHandler {

    // PROPERTIES
    private Keyboard keyboard;
    private Picture playerImage;
    private Direction currentDirection;
    private Sound sound;

    // CONSTRUCTOR

    public Player(int health,Game game, int xPosition, int yPosition) {

        super(health,game,xPosition,yPosition);
        playerImage = new Picture(xPosition, yPosition, "resources/images/enemy.png");
        this.keyboard = new Keyboard(this);

        speed = 0;
    }

    // METHODS

    public void move() {
        accelerate(currentDirection, speed);
    }

    public void accelerate(Direction direction, int speed) {
        for (int i = 0; i < speed; i++) {
            moveInDirection(direction, 1);
        }
    }

    public void moveInDirection(Direction direction, int distance) {

        switch (direction) {

            case LEFT:
                moveLeft(distance);
                break;
            case RIGHT:
                moveRight(distance);
                break;
        }
    }

    @Override
    public void suffer(int damage) {
        sound = new Sound("/resources/sounds/explosion.wav");
        if(health - damage == 0) {

            sound.play(true);
            this.setDestroyed();
            playerImage.delete();
        }
        health -= damage;
        sound.play(true);
    }

    public void  init() {

        playerImage.draw();
        KeyboardEvent leftPressed = new KeyboardEvent();
        leftPressed.setKey(KEY_LEFT);
        leftPressed.setKeyboardEventType(KEY_PRESSED);

        KeyboardEvent rightPressed = new KeyboardEvent();
        rightPressed.setKey(KEY_RIGHT);
        rightPressed.setKeyboardEventType(KEY_PRESSED);

        KeyboardEvent leftReleased = new KeyboardEvent();
        leftReleased.setKey(KEY_LEFT);
        leftReleased.setKeyboardEventType(KEY_RELEASED);

        KeyboardEvent rightReleased = new KeyboardEvent();
        rightReleased.setKey(KEY_RIGHT);
        rightReleased.setKeyboardEventType(KEY_RELEASED);

        KeyboardEvent shoot  = new KeyboardEvent();
        shoot.setKey(KeyboardEvent.KEY_SPACE);
        shoot.setKeyboardEventType(KEY_PRESSED);

        keyboard.addEventListener(leftPressed);
        keyboard.addEventListener(rightPressed);
        keyboard.addEventListener(leftReleased);
        keyboard.addEventListener(rightReleased);
        keyboard.addEventListener(shoot);

    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {

            case KEY_LEFT:
                currentDirection = Direction.LEFT;
                speed = 5;
                break;

            case KEY_RIGHT:
                currentDirection = Direction.RIGHT;
                speed = 5;
                break;

            case KeyboardEvent.KEY_SPACE:
                shoot();
                break;

        }

    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

        speed = 0;
    }

    private void moveRight(int distance) {
        if(getxPosition()+distance < game.getFieldWidth()-80) {
            playerImage.translate(distance, 0);
            xPosition += distance;
        }
    }


    private void moveLeft(int distance) {
        if(getxPosition()-distance > 10) {
            playerImage.translate(-distance, 0);
            xPosition -= distance;
        }
    }

    public void shoot() {
        if(!isDestroyed()) {
            sound = new Sound("/resources/sounds/playershoot.wav");
            sound.play(true);
            game.addBullet(this);
        }
    }



}
