package org.academiadecodigo.stringteasers.academyinvaders.gameobject.character;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.stringteasers.academyinvaders.Game;
import org.academiadecodigo.stringteasers.academyinvaders.sounds.Sound;

public class Enemy extends Character {

    private final static int SPEED = 2;
    private Picture enemyImage;
    private EnemyDirection direction;
    private boolean initiated;
    private Sound sound;


    public Enemy(int health, Game game, int xPosition, int yPosition) {
        super(health,game,xPosition,yPosition);
        direction = EnemyDirection.values()[(int) (Math.random() * EnemyDirection.values().length)]; // random direction at when
        enemyImage = new Picture(xPosition,yPosition, "resources/images/player.png");

    }

    public void init() {
        enemyImage.draw();
        move();
    }

    @Override
    public void move() {

        if(isDestroyed()) {
            return;
        }

        if(isHittingWall()) {
            direction =  direction.opposite();
            return;
        }

        accelerate(direction, SPEED);
        shoot();
    }

    @Override
    public void suffer(int damage) {

        if(health - damage == 0) {
            this.setDestroyed();
            enemyImage.delete();

        }

        health -= damage;

    }

    public void shoot() {

        int random = (int) (Math.random() * 100);
        sound = new Sound("/resources/sounds/pedroshoot.wav");

        if (random < 5) {

            game.addEnemyBullet(this);
            sound.play(true);
        }

    }
    public void setInitiated(){
        initiated=true;
    }

    public boolean isInitiated() {
        return initiated;
    }

    private void accelerate(EnemyDirection direction, int speed) {

        for (int i = 0; i < speed; i++) {
            if(direction.equals(EnemyDirection.LEFT)) {
                enemyImage.translate(-1, 0);
                xPosition--;

            }

            if(direction.equals(EnemyDirection.RIGHT)) {
                enemyImage.translate(1, 0);
                xPosition++;

            }
        }
    }

    /**
     * Verify if is near the wall
     * @return true when reach near limit
     */
    private boolean isHittingWall() {

        switch (direction) {
            case LEFT:
                if ( xPosition < 75) {
                    return true;
                }
                break;

            case RIGHT:
                if (xPosition > 670) {
                    return true;
                }
                break;

        }

        return false;
    }

}
