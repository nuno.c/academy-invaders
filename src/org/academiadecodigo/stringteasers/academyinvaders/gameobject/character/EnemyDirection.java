package org.academiadecodigo.stringteasers.academyinvaders.gameobject.character;

public enum EnemyDirection {
    LEFT,
    RIGHT;

    public EnemyDirection opposite() {

        EnemyDirection opposite = null;

        switch (this) {
            case LEFT:
                opposite = EnemyDirection.RIGHT;
                break;
            case RIGHT:
                opposite = EnemyDirection.LEFT;
                break;
        }

        return opposite;
    }

}
