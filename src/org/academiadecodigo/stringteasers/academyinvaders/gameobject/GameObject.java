package org.academiadecodigo.stringteasers.academyinvaders.gameobject;

import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Ammunition;
import org.academiadecodigo.stringteasers.academyinvaders.interfaces.Hitable;
import org.academiadecodigo.stringteasers.academyinvaders.interfaces.Movable;

public abstract class GameObject implements Hitable, Movable {

    private boolean destroyed;
    // missing picture

    /*
     * Construtor
     */
    public GameObject() {
        this.destroyed = false;
    }



    @Override
    public void move() {

    }


    /*
     * Getters & Setters
     */
    public boolean isDestroyed(){
        return this.destroyed;
    }


    public void setDestroyed(){
        this.destroyed = true;
    }
}
