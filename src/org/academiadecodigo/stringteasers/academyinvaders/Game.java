package org.academiadecodigo.stringteasers.academyinvaders;

import org.academiadecodigo.stringteasers.academyinvaders.ammunition.Bullet;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Enemy;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Meteor;
import org.academiadecodigo.stringteasers.academyinvaders.gameobject.character.Player;
import org.academiadecodigo.stringteasers.academyinvaders.sgf.Field;
import org.academiadecodigo.stringteasers.academyinvaders.sounds.Sound;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Game {

    // PROPERTIES

    private Field gameField;
    private Player player;
    private Enemy enemy;
    private int delay;
    private List<Bullet> bulletList;
    private List<Bullet> enemyBulletList;
    private CollisionDetector collisionDetector;
    private Meteor[] meteors;
    private int meteorsLaunched;
    private Sound sound;


    // CONSTRUCTOR

    public Game(int delay) throws InterruptedException {

        this.delay = delay;
        gameField = new Field();
        player = new Player(100, this, 460, 800);
        bulletList = new CopyOnWriteArrayList<>();
        enemy = new Enemy(100, this, 150, 100);
        enemyBulletList = new ArrayList<>();
        meteors = new Meteor[40];
        meteorsLaunched = 0;
        sound = new Sound("/resources/sounds/starwars.wav");
        collisionDetector = new CollisionDetector(player, bulletList, enemy, enemyBulletList, meteors);
    }

    // METHODS

    public void addEnemyBullet(Enemy enemy) {
        Bullet bullet = new Bullet(enemy, enemy.getxPosition(), enemy.getyPosition());
        enemyBulletList.add(bullet);
    }

    public void addBullet(Player player) {
        Bullet bullet = new Bullet(player, player.getxPosition(), player.getyPosition());
        bulletList.add(bullet);
    }

    public void init() {

        gameField.init();
        sound.play(true);
        createMeteors();
        player.init();
        createMeteors();
    }

    private void createMeteors() {

        for (int i = 0; i < meteors.length ; i++) {
            int randomXPosition = (int) (Math.random() * ((gameField.getRightBorder() - 80) - gameField.getLeftBorder() + 1) + (gameField.getLeftBorder()));
            meteors[i] = new Meteor(randomXPosition, -80);
        }

        for (Meteor met: this.meteors
             ) {
            System.out.println(met);
        }
    }

    private void insertMeteors() {

        int randomMeteor = (int) (Math.random() * (101));
        int randomSpeed = (int) (Math.random() * (10 - 2 + 1) + 2);
        if (randomMeteor < 2 && meteorsLaunched < meteors.length - 1) {
            meteors[meteorsLaunched].setSpeed(randomSpeed);
            meteorsLaunched++;
        }

        //collisionDetector = new CollisionDetector(player, bulletList, enemy, enemyBulletList, meteors);
    }

    public int getFieldWidth() {
        return gameField.WIDTH;
    }

    public void start() throws InterruptedException {


        while (true) {

            Thread.sleep(delay);

            for (Bullet bullet : bulletList) {
                bullet.motion();
                if (bullet.getyPosition() - 10 <= 10) {
                    bullet.deleteImage();
                }
                if (bullet.getDestroyed()) {bullet.deleteImage();}
            }

            for (Bullet bullet : enemyBulletList) {

                bullet.motionEnemy();
                if (bullet.getyPosition() + 20 >= gameField.HEIGHT) {
                    bullet.deleteImage();
                }
                if (bullet.getDestroyed()) {bullet.deleteImage();}

            }

            for (int a = 0; a < meteors.length - 1; a++) {
                meteors[a].move();
            }

            insertMeteors();

            for (int b = 0; b < meteors.length - 1; b++) {

                if (meteors[b].getYPosition() + 80 >= gameField.HEIGHT) {
                    meteors[b].deleteImage();
                }
            }

            player.move();

            if (meteors[meteors.length - 2].getYPosition() > 600 && !enemy.isInitiated()) {
                enemy.init();
                enemy.setInitiated();
            }

            if (enemy.isInitiated()) {
                enemy.move();
            }
            collisionDetector.isMeteorHitPlayer();

            collisionDetector.isPlayerHit();

            collisionDetector.isEnemyHit();

            collisionDetector.isMeteorHit();

            if (player.isDestroyed()) {gameField.endField();}

        }
    }





}
